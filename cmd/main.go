package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/url"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
	"github.com/spf13/viper"

	_weaherHttpDelivery "github.com/talama014/weatherApi/weather/delivery/http"
	_weatherHttpDeliveryMiddleware "github.com/talama014/weatherApi/weather/delivery/http/middleware"
	_weatherRepo "github.com/talama014/weatherApi/weather/repository/mysql"
	_weatherUsecase "github.com/talama014/weatherApi/weather/usecase"
)

func init() {
	viper.SetConfigFile(`config.json`)
	err := viper.ReadInConfig()

	if err != nil {
		panic(err)
	}

	if viper.GetBool(`debug`) {
		log.Println("DEBUG")
	}
}

func main() {

	dbHost := viper.GetString(`database.host`)
	dbPort := viper.GetString(`database.port`)
	dbUser := viper.GetString(`database.user`)
	dbPass := viper.GetString(`database.pass`)
	dbName := viper.GetString(`database.name`)
	connection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", dbUser, dbPass, dbHost, dbPort, dbName)

	val := url.Values{}
	//root:example@tcp(127.0.0.1:3306)/weatherapi?charset=utf8mb4&parseTime=True&loc=Local
	val.Add("parseTime", "1")
	val.Add("loc", "Local")
	dsn := fmt.Sprintf("%s?%s", connection, val.Encode())
	dbConn, err := sql.Open(`mysql`, dsn)

	if err != nil {
		log.Fatal(err)
	}
	err = dbConn.Ping()
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		err := dbConn.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	e := echo.New()
	middL := _weatherHttpDeliveryMiddleware.InitMiddleware()
	e.Use(middL.CORS)

	ar := _weatherRepo.NewMysqlWeatherRepository(dbConn)

	timeoutContext := time.Duration(viper.GetInt("context.timeout")) * time.Second
	au := _weatherUsecase.NewWeatherUsecase(ar, timeoutContext)
	_weaherHttpDelivery.NewWeatherHandler(e, au)

	log.Fatal(e.Start(viper.GetString("server.address")))

}
