package domain

import (
	"context"
)

type Weather struct {
	ID uint64 `json:"-"`
	//ngay
	Date string `json:"Ngày" validate:"required"`
	//do am
	Humidity uint `json:"Độ ẩm %:"`
	//toc do gio
	WindSpeed float64 `json:"Tốc độ gió m/s:"`
	//api se lay ra do K
	TempMax int32 `json:"Nhiệt độ cao nhất C: "`
	TempMin int32 `json:"Nhiệt độ thấp nhất C:"`
}

// WeatherUsecase represent the weather's usecases
type WeatherUsecase interface {
	GetByID(ctx context.Context, id int64) (Weather, error)
	Store(ctx context.Context, a *Weather) error
	GetByDate(ctx context.Context, date string) (Weather, error)
	// Update(ctx context.Context, ar *Weather) error
	// Delete(ctx context.Context, id int64) error
}

// WeatherRepository represent the weather's repository
type WeatherRepository interface {
	GetByID(ctx context.Context, id int64) (Weather, error)
	Store(ctx context.Context, a *Weather) error
	GetByDate(ctx context.Context, date string) (Weather, error)
	// Update(ctx context.Context, ar *Weather) error
	// Delete(ctx context.Context, id int64) error
}
