package usecase

import (
	"context"
	"time"

	"github.com/talama014/weatherApi/domain"
)

type weatherUsecase struct {
	weatherRepo    domain.WeatherRepository
	contextTimeout time.Duration
}

func NewWeatherUsecase(w domain.WeatherRepository, timeout time.Duration) domain.WeatherRepository {
	return &weatherUsecase{
		weatherRepo:    w,
		contextTimeout: timeout,
	}
}

func (a *weatherUsecase) GetByID(c context.Context, id int64) (res domain.Weather, err error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	res, err = a.weatherRepo.GetByID(ctx, id)
	if err != nil {
		return
	}
	return
}

func (a *weatherUsecase) Store(c context.Context, m *domain.Weather) (err error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	existedWeather, _ := a.GetByID(ctx, int64(m.ID))
	if existedWeather != (domain.Weather{}) {
		return domain.ErrConflict
	}
	existedWeatherDate, _ := a.GetByDate(ctx, m.Date)
	if existedWeatherDate != (domain.Weather{}) {
		return domain.ErrConflict
	}

	err = a.weatherRepo.Store(ctx, m)
	return
}
func (a *weatherUsecase) GetByDate(c context.Context, date string) (res domain.Weather, err error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	res, err = a.weatherRepo.GetByDate(ctx, date)
	if err != nil {
		return
	}
	return
}
