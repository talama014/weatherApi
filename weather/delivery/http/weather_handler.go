package http

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"github.com/sirupsen/logrus"
	"github.com/talama014/weatherApi/domain"
	validator "gopkg.in/go-playground/validator.v9"
)

type ResponseError struct {
	Message string `json:"message"`
}

type WeatherHandler struct {
	AUsecase domain.WeatherUsecase
}

func NewWeatherHandler(e *echo.Echo, us domain.WeatherUsecase) {
	handler := &WeatherHandler{
		AUsecase: us,
	}
	e.GET("/getweatherbyid/:id", handler.GetByID)
	e.GET("/getweatherbydate/:date", handler.GetByDate)
	e.POST("/weather", handler.Store)
}

func (a *WeatherHandler) GetByID(c echo.Context) error {
	idP, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusNotFound, domain.ErrNotFound.Error())
	}

	id := int64(idP)
	ctx := c.Request().Context()

	wea, err := a.AUsecase.GetByID(ctx, id)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusOK, wea)
}

func (a *WeatherHandler) GetByDate(c echo.Context) error {
	date := c.Param("date")

	ctx := c.Request().Context()

	wea, err := a.AUsecase.GetByDate(ctx, date)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusOK, wea)
}
func isRequestValid(m *domain.Weather) (bool, error) {
	validate := validator.New()
	err := validate.Struct(m)
	if err != nil {
		return false, err
	}
	return true, nil
}
func (a *WeatherHandler) Store(c echo.Context) (err error) {
	var weather domain.Weather
	err = c.Bind(&weather)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err.Error())
	}

	var ok bool
	if ok, err = isRequestValid(&weather); !ok {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	ctx := c.Request().Context()
	err = a.AUsecase.Store(ctx, &weather)
	if err != nil {
		return c.JSON(getStatusCode(err), ResponseError{Message: err.Error()})
	}

	return c.JSON(http.StatusCreated, weather)
}

func getStatusCode(err error) int {
	if err == nil {
		return http.StatusOK
	}
	logrus.Error(err)
	switch err {
	case domain.ErrInternalServerError:
		return http.StatusInternalServerError
	case domain.ErrNotFound:
		return http.StatusNotFound
	case domain.ErrConflict:
		return http.StatusConflict
	default:
		return http.StatusInternalServerError
	}
}
