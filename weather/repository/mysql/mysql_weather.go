package mysql

import (
	"context"
	"database/sql"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/talama014/weatherApi/domain"
)

type mysqlWeatherRepository struct {
	Conn *sql.DB
}

func NewMysqlWeatherRepository(Conn *sql.DB) domain.WeatherRepository {
	return &mysqlWeatherRepository{Conn}
}

func (m *mysqlWeatherRepository) fetch(ctx context.Context, query string, args ...interface{}) (result []domain.Weather, err error) {
	rows, err := m.Conn.QueryContext(ctx, query, args...)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	defer func() {
		errRow := rows.Close()
		if errRow != nil {
			logrus.Error(errRow)
		}
	}()
	result = make([]domain.Weather, 0)
	for rows.Next() {
		t := domain.Weather{}
		err = rows.Scan(
			&t.ID,
			&t.Date,
			&t.Humidity,
			&t.WindSpeed,
			&t.TempMax,
			&t.TempMin,
		)
		if err != nil {
			logrus.Error(err)
			return nil, err
		}
		result = append(result, t)
	}
	return result, nil
}

func (m *mysqlWeatherRepository) GetByID(ctx context.Context, id int64) (res domain.Weather, err error) {
	query := `SELECT id,date,humidity, windspeed, tempmax, tempmin
															FROM weather WHERE ID = ?`

	list, err := m.fetch(ctx, query, id)
	if err != nil {
		return domain.Weather{}, err
	}

	if len(list) > 0 {
		res = list[0]
	} else {
		return res, domain.ErrNotFound
	}

	return
}
func (m *mysqlWeatherRepository) Store(ctx context.Context, a *domain.Weather) (err error) {
	query := `INSERT  weather SET date=? , humidity=? , windspeed=?, tempmax=? , tempmin=?`
	stmt, err := m.Conn.PrepareContext(ctx, query)
	if err != nil {
		return
	}

	res, err := stmt.ExecContext(ctx, a.Date, a.Humidity, a.WindSpeed, a.TempMax, a.TempMin)
	if err != nil {
		return
	}
	lastID, err := res.LastInsertId()
	if err != nil {
		return
	}
	a.ID = uint64(lastID)
	return
}
func (m *mysqlWeatherRepository) GetByDate(ctx context.Context, date string) (res domain.Weather, err error) {
	query := `SELECT id,date,humidity, windspeed, tempmax, tempmin
  						FROM weather WHERE date = ?`

	list, err := m.fetch(ctx, query, date)
	if err != nil {
		return
	}
	if len(list) > 0 {
		res = list[0]
	} else {
		m.StoreApi(ctx)
		return res, domain.ErrConflict
	}
	return
}
func GetSevenDayWeather() (date []string, humidity []int, windspeed []int, tempmax []int, tempmin []int) {
	url := "https://foreca-weather.p.rapidapi.com/forecast/daily/102643743?alt=0&tempunit=C&windunit=MS&periods=8&dataset=full"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("x-rapidapi-host", "foreca-weather.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", "8d4dd5895dmsh9db66057d31d2f5p1b8a3fjsnb343a7dbb365")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	data := strings.Split(string(body), "date")

	humiditymax := 0
	humiditymin := 0

	for i := 1; i < len(data)-1; i++ {
		data2 := strings.Split(string(data[i]), ",")
		for j := 0; j < len(data2)-1; j++ {
			if j == 0 {
				val := strings.Split(data2[j], ":")
				val2 := val[1]
				date = append(date, val2[1:len(val2)-1])
			}
			if j == 3 {
				val := strings.Split(data2[j], ":")
				tm, _ := strconv.Atoi(val[1])
				tempmax = append(tempmax, tm)
			}
			if j == 4 {
				val := strings.Split(data2[j], ":")
				tm, _ := strconv.Atoi(val[1])
				tempmin = append(tempmin, tm)

			}
			if j == 7 {
				val := strings.Split(data2[j], ":")
				humiditymax, _ = strconv.Atoi(val[1])
			}
			if j == 8 {
				val := strings.Split(data2[j], ":")
				humiditymax, _ = strconv.Atoi(val[1])
			}
			if j == 9 {
				humidity = append(humidity, int((humiditymax+humiditymin)/2))
				humiditymax = 0
				humiditymin = 0
			}
			if j == 13 {
				val := strings.Split(data2[j], ":")
				tm, _ := strconv.Atoi(val[1])
				windspeed = append(windspeed, tm)
			}
		}
	}
	return

}
func (m *mysqlWeatherRepository) StoreApi(ctx context.Context) (err error) {
	query := `INSERT  weather SET date=? , humidity=? , windspeed=?, tempmax=? , tempmin=?`
	stmt, err := m.Conn.PrepareContext(ctx, query)
	if err != nil {
		return
	}
	date := []string{}
	humidity := []int{}
	windspeed := []int{}
	tempmax := []int{}
	tempmin := []int{}
	date, humidity, windspeed, tempmax, tempmin = GetSevenDayWeather()

	for i := 0; i < len(date)-1; i++ {
		_, err := stmt.ExecContext(ctx, date[i], humidity[i], windspeed[i], tempmax[i], tempmin[i])
		if err != nil {
			break
		}
	}
	return
}
